//
//  ViewController.m
//  GestureRecognizerDemo
//
//  Created by James Cash on 14-06-18.
//  Copyright © 2018 Occasionally Cogent. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@property (nonatomic,strong) UIView *basketView;
@property (nonatomic,strong) UIView *draggingView;
@property (nonatomic,strong) UIView *targetView;
@property (nonatomic,strong) NSDate *lastChangeAt;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.lastChangeAt = [NSDate dateWithTimeIntervalSince1970:0];

    self.basketView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 150, 150)];
    self.basketView.backgroundColor = UIColor.purpleColor;
    [self.view addSubview:self.basketView];

    [self.basketView addGestureRecognizer:
     [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(petBasket:)]];

    self.targetView = [[UIView alloc] initWithFrame:CGRectZero];
    self.targetView.backgroundColor = UIColor.greenColor;
    [self.view addSubview:self.targetView];
    self.targetView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.targetView.trailingAnchor constraintEqualToAnchor:self.view.trailingAnchor].active = YES;
    [self.targetView.bottomAnchor constraintEqualToAnchor:self.view.bottomAnchor].active = YES;
    [self.targetView.widthAnchor constraintEqualToConstant:150].active = YES;
    [self.targetView.heightAnchor constraintEqualToConstant:150].active = YES;

    UILongPressGestureRecognizer* lpgr = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPressOnMovingView:)];
    [self.basketView addGestureRecognizer:lpgr];
    // Don't need to do this in this case, but some UIView subclasses (in particular, UIIMageView) have userInteractionEnabled = NO by default
    // this means that they don't handle touch events on them (just pass them to their parent), but that means that they don't forward the touches to their gesture recognizers
    // so the recognizer action would never fire
    self.basketView.userInteractionEnabled = YES;

    // NB: make sure you use the *scheduled*TimerWith ... method
    // the timerWith... methods create a timer but you need to also make your own runloop to put it in
    // ...which we don't really want to do here: Our app already has a runloop
    [NSTimer scheduledTimerWithTimeInterval:1 repeats:YES block:^(NSTimer * _Nonnull timer) {
        NSLog(@"I love blocks, so much nicer than target-action >_<");
    }];
}

// Important to understand:
// this method will be called many times as the gesture moves:
// it gets called first when the gesture begins (state = Began)
// it gets called again every time the gesture "changes" (moves) (state = Changed)
// it will be called once more when the gesture ends (state = Ended)
- (void)longPressOnMovingView:(UILongPressGestureRecognizer*)recognizer
{
//    NSLog(@"saw long press");
    switch (recognizer.state) {
        case UIGestureRecognizerStateBegan:
        {
            NSLog(@"Long press began");
            self.draggingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 100, 100)];
            self.draggingView.backgroundColor = UIColor.redColor;
            [self.view addSubview:self.draggingView];
            self.draggingView.center = [recognizer locationInView:self.view];
        }
            break;
        case UIGestureRecognizerStateChanged:
        {
            // in general, when getting the location of a touch to position a view
            // we probably want to get the locationInView of the *parent/superview* of the view we want to move
            // this is because views are positioned relative to their parent
            CGPoint touchCenter = [recognizer locationInView:self.view];
            self.draggingView.center = touchCenter;
        }
            break;
        case UIGestureRecognizerStateEnded:
        {
            // a number of different ways of determining success
            // 1. does the frame of the dragging view overlap with the frame of the target view at all?
            BOOL hitTarget = CGRectIntersectsRect(self.draggingView.frame, self.targetView.frame);
            // 2. Is the center point of the dragging view inside the frame of the target view at all?
//            BOOL hitTarget = CGRectContainsPoint(self.targetView.frame, self.draggingView.center);
            // 3. Is the dragging view fully inside the target view
//            BOOL hitTarget = CGRectContainsRect(self.targetView.frame, self.draggingView.frame);
            if (hitTarget) {
                CGFloat π = M_PI;
                CGFloat τ = 2 * π;
                [UIView animateWithDuration:2 animations:^{
                    self.draggingView.transform = CGAffineTransformMakeRotation(τ);
                    self.draggingView.alpha = 0;
                } completion:^(BOOL finished) {
                    [self.draggingView removeFromSuperview];
                    self.draggingView = nil;
                }];
            } else {
                [UIView animateWithDuration:1.5 animations:^{
                    self.draggingView.center =
                    CGPointMake(self.draggingView.center.x,
                                CGRectGetMaxY(self.view.frame) +
                                CGRectGetWidth(self.draggingView.frame));
                } completion:^(BOOL finished) {
                    [self.draggingView removeFromSuperview];
                    self.draggingView = nil;
                }];
            }
        }
            break;
        default:
            NSLog(@"Something else happend");
            break;
    }
}

- (void)petBasket:(UIPanGestureRecognizer*)sender
{
    if (sender.state == UIGestureRecognizerStateChanged) {
        CGPoint velocity = [sender velocityInView:self.basketView];
        [sender setTranslation:CGPointZero inView:self.basketView];
        double speed = fabs(hypot(velocity.x, velocity.y));
        NSDate *now = [NSDate date];
        if ([now timeIntervalSinceDate:self.lastChangeAt] > 0.5) {
            if (speed > 500) {
                // "angry"
                self.basketView.backgroundColor = UIColor.redColor;
            } else {
                // "not angry"
                self.basketView.backgroundColor = UIColor.purpleColor;
            }
            self.lastChangeAt = now;
        }
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
